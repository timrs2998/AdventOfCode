#!/usr/bin/env node
'use strict'

const registers = {a: 0, b: 0}

const instructions = `
jio a, +19
inc a
tpl a
inc a
tpl a
inc a
tpl a
tpl a
inc a
inc a
tpl a
tpl a
inc a
inc a
tpl a
inc a
inc a
tpl a
jmp +23
tpl a
tpl a
inc a
inc a
tpl a
inc a
inc a
tpl a
inc a
tpl a
inc a
tpl a
inc a
tpl a
inc a
inc a
tpl a
inc a
inc a
tpl a
tpl a
inc a
jio a, +8
inc b
jie a, +4
tpl a
inc a
jmp +2
hlf a
jmp -7
`

const memory = instructions.trim().split(/\n/g)

function execute(memory, registers) {
    for (let i = 0; i < memory.length;) {
        const line = memory[i]

        let m = undefined;
        if (m = line.match(/^hlf ([ab])$/)) {
            registers[m[1]] /= 2
            i += 1
        } else if (m = line.match(/^tpl ([ab])$/)) {
            registers[m[1]] *= 3
            i += 1
        } else if (m = line.match(/^inc ([ab])$/)) {
            registers[m[1]] += 1
            i += 1
        } else if (m = line.match(/^jmp ([+-]\d+)$/)) {
            i += parseInt(m[1])
        } else if (m = line.match(/^jie ([ab]), ([+-]\d+)$/)) {
            if (registers[m[1]] % 2 === 0) {
                i += parseInt(m[2])
            } else {
                i += 1
            }
        } else if (m = line.match(/^jio ([ab]), ([+-]\d+)$/)) {
            if (registers[m[1]] === 1) {
                i += parseInt(m[2])
            } else {
                i += 1
            }
        } else {
            throw 'Illegal instruction'
        }
    }
}

execute(memory, registers)
console.log('Part 1: ', registers)

registers.a = 1
registers.b = 0

execute(memory, registers)
console.log('Part 2: ', registers)
