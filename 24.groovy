#!/usr/bin/env groovy
import groovy.transform.Memoized

class Util {
    /** For our problem input */
    final static Set<Integer> WEIGHTS = [
            1, 3, 5, 11, 13, 17, 19, 23, 29, 31, 41, 43, 47, 53, 59,
            61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113
    ].toSet().asImmutable()

//    /** Example given in problem **/
//    final static Set<Integer> WEIGHTS = [1, 2, 3, 4, 5, 7, 8, 9, 10, 11].toSet().asImmutable()

    final static TOTAL_WEIGHT = WEIGHTS.sum()

    @Memoized
    static BigInteger factorial(int n) {
        n <= 1 ? 1 : n * factorial(n - 1)
    }

    static Set<Set<Integer>> subsets(Set<Integer> set, int size) {
        BigInteger nCrK = factorial(set.size()) / factorial(size) / factorial(set.size() - size)
        println "Looping through $nCrK sets"
        Set<Set<Integer>> res = [] as Set
        subsets(set.toList(), size, 0, [] as Set, res)
        return res
    }

    static void subsets(List<Integer> set, int size, int i, Set<Integer> current, Set<Set<Integer>> solution) {
        if (current.size() == size) {
            if (current.sum() == TOTAL_WEIGHT / 4) { // FOR PART ONE, CHANGE TO 3, FOR PART TWO, CHANGE TO 4
                solution.add(new HashSet<>(current))
            }
            return
        } else if (i == set.size()) {
            return
        }
        Integer x = set[i]
        current << x
        subsets(set, size, i+1, current, solution)
        current.remove(x)
        subsets(set, size, i+1, current, solution)
    }
}

for (int i = 0; i < Util.WEIGHTS.size(); i++) {
    Set<Set<Integer>> sets = Util.subsets(Util.WEIGHTS, i)
    if (sets) {
        println "Size: ${sets.size()}"
        println 'Enganglement: '
        println sets.collect { it.inject(1) { prod, val -> (long) prod * (long) val } }.min()
        break
    }
}